export interface Category {
  _id: string;
  name: string;
  created_at: Date;
  updatedAt: Date;
}
