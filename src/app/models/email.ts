import { Category } from "./category";

export interface Email {
  _id: string;
  emailId: string;
  category: any;
  created_at: Date;
  updatedAt: Date;
}
