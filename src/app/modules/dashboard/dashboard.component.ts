import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';
import { Email } from 'src/app/models/email';
import { Category } from 'src/app/models/category';
import { LoginService } from 'src/app/services/login.service';
import { EmailService } from 'src/app/services/email.service';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public emails$: Observable<Array<Email>>;
  public categories$: Observable<Array<Category>>;
  public user: User;
  public emailView = true;
  public disableEmailView = false;
  constructor(
    private _emailService: EmailService,
    private _categoryService: CategoryService,
    private _loginService: LoginService,
  ) {
    this.emails$ = this._emailService.getAll();
    this.categories$ = this._categoryService.getAllCategories();
    this._loginService.currentUser.subscribe(cu => this.user = cu);
  }

  ngOnInit() {
    this.categories$.subscribe(data => {
      if (!data || !data.length) {
        this.emailView = false;
        this.disableEmailView = true;
      } else {
        this.disableEmailView = false;
      }
    });
  }

  logout() {
    this._loginService.logout();
  }

  updateEmail(email: Email) {
    console.log(email);
    this._emailService.update(email._id, email)
      .subscribe(data => {
        this.reload();
      });
  }

  updateCategory(category: Category) {
    this._categoryService.update(category._id, category).subscribe(data => {
      this.reload();
    });
  }

  deleteEmail(email: Email) {
    this._emailService.delete(email._id).subscribe(data => {
      this.reload();
    });
  }

  deleteCategory(category: Category) {
    this._categoryService.delete(category._id).subscribe(data => {
      this.reload();
    });
  }

  saveNewEmail(email) {
    this._emailService.create(email).subscribe(data => {
      this.reload();
    });
  }

  saveNewCatgory(category) {
    this._categoryService.create(category).subscribe(data => {
      this.reload();
    });
  }

  reload() {
    this.emails$ = this._emailService.getAll();
    this.categories$ = this._categoryService.getAllCategories();
    this.categories$.subscribe(data => {
      if (!data || !data.length) {
        this.emailView = false;
        this.disableEmailView = true;
      } else {
        this.disableEmailView = false;
      }
    });
  }

}
