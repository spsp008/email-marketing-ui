import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from 'src/app/models/category';
import { Email } from 'src/app/models/email';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  @Input()
  categories: Array<Category>;

  @Input()
  emails: Array<Email>;

  @Output()
  onUpdateEmail: EventEmitter<Email> = new EventEmitter<Email>();

  @Output()
  onUpdateCategory: EventEmitter<Category> = new EventEmitter<Category>();

  @Output()
  onDeleteEmail: EventEmitter<Email> = new EventEmitter<Email>();

  @Output()
  onDeleteCategory: EventEmitter<Category> = new EventEmitter<Category>();

  @Output()
  onNewEmail: EventEmitter<Email> = new EventEmitter<Email>();

  @Output()
  onNewCategory: EventEmitter<Category> = new EventEmitter<Category>();

  public editingCategory: Category;
  public newCategory;

  constructor() {}
  ngOnInit() {

  }

  editCategory(category: Category) {
    this.editingCategory =  category;
  }

  saveCategory(category: Category) {
    this.editingCategory =  null;
    this.onUpdateCategory.emit(category);
  }

  deleteCategory(category: Category) {
    this.editingCategory =  null;
    this.onDeleteCategory.emit(category);
  }

  initNew() {
    if (this.newCategory) return;
    this.newCategory = {
      name: null
    };
  }

  saveNew() {
    this.onNewCategory.emit(this.newCategory);
    this.newCategory = null;
  }

  cancelNew() {
    this.newCategory = null;
  }

  trackByFn(index, item) {
    return item._id;
  }
}
