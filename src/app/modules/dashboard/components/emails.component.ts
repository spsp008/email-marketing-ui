import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Category } from 'src/app/models/category';
import { Email } from 'src/app/models/email';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-emails',
  templateUrl: './emails.component.html',
  styleUrls: ['./emails.component.scss']
})
export class EmailsComponent implements OnInit {
  constructor() {}
  @Input()
  emails: Array<Email>;

  @Output()
  onUpdateEmail: EventEmitter<Email> = new EventEmitter<Email>();

  @Output()
  onDeleteEmail: EventEmitter<Email> = new EventEmitter<Email>();

  @Output()
  onNewEmail: EventEmitter<Email> = new EventEmitter<Email>();

  @Input()
  categories: Array<Category>;

  @Input()
  category: Category;

  public editingEmail;
  public newEmail;

  ngOnInit() {

  }

  editEmail(email: Email) {
    this.editingEmail =  {...email, category: email.category._id};
  }

  saveEmail(email: Email) {
    this.editingEmail =  null;
    this.onUpdateEmail.emit(email);
  }

  deleteEmail(email: Email) {
    this.editingEmail =  null;
    this.onDeleteEmail.emit(email);
  }

  trackByFn(index, item) {
    return item._id;
  }

  initNew() {
    if (this.newEmail) return;

    this.newEmail = {
      emailId: null,
      category: this.category ? this.category._id : this.categories[0]?._id
    }
  }

  saveNew() {
    this.onNewEmail.emit(this.newEmail);
    this.newEmail = null;
  }

  cancelNew() {
    this.newEmail = null;
  }
}
