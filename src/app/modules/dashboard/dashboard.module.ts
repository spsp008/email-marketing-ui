import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EmailCategoryPipe } from '../pipes/email-category.pipe';
import { CategoriesComponent } from './components/categories.component';
import { EmailsComponent } from './components/emails.component';
import { DashboardComponent } from './dashboard.component';

@NgModule({
  declarations: [
    DashboardComponent,
    EmailsComponent,
    CategoriesComponent,
    EmailCategoryPipe
  ],
  imports: [CommonModule, FormsModule],
  providers: []
})
export class DashboardModule { }
