import { Pipe, PipeTransform } from '@angular/core';
import { Category } from 'src/app/models/category';
import { Email } from 'src/app/models/email';

@Pipe({name: 'emailCategory'})

export class EmailCategoryPipe implements PipeTransform {
  transform(emails: Array<Email>, category: Category): Array<Email> {
    if (!emails) return [];
    if (!category) return emails;

    return emails.filter(e => e.category._id === category._id);
  }
}
