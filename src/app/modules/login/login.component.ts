import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public router: Router, private _loginService: LoginService) {
    if (this._loginService.currentUserValue) {
      this.router.navigate(['/dashboard']);
    }
  }
  public isLogin = true;
  public username: string;
  public password: string;
  public email: string;

  ngOnInit() {

  }

  login() {
    const {email, password} =  this;
    this._loginService.login({email, password}).subscribe(res => {
      if (res) {
        this.router.navigate(['/dashboard']);
      } else {
        alert(res.message);
      }
    });
  }

  signUp() {
    const {username, email, password} =  this;
    if (!password || password.length < 8) {
      alert('Password length must be greater than equal to 8');
      return;
    }
    this._loginService.signup({username, email, password}).subscribe(res => {
      if (res)  {
        this.router.navigate(['/dashboard']);
        this.isLogin = true;
      } else {
        alert(res.msg);
      }
    });
  }
}
