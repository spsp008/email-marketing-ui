import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class BaseHttpService {
  private baseURL = 'http://localhost:3000';

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(public httpClient: HttpClient) {}

  get(apiURL) {
    return this.httpClient.get(this.baseURL + apiURL, this.httpOptions);
  }

  post(apiURL, data, options?: any) {
    return this.httpClient.post(this.baseURL + apiURL, data, options || this.httpOptions);
  }

  patch(apiURL, data) {
    return this.httpClient.patch(this.baseURL + apiURL, data, this.httpOptions);
  }

  delete(apiURL) {
    return this.httpClient.delete(this.baseURL + apiURL, this.httpOptions);
  }

  put(apiURL, data) {
    return this.httpClient.put(this.baseURL + apiURL, data, this.httpOptions);
  }
}
