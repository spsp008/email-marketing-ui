import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { catchError, map} from 'rxjs/operators';
import { of } from 'rxjs';
import { User } from '../models/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private baseApi: BaseHttpService, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  login(data: Partial<User>): any {
    return this.baseApi.post(`/user/login`, data).pipe(
      map((resp: any) => {
        if (resp.success) {
          const {user} = resp;
          localStorage.setItem('token', user.token);
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
          return resp.user;
        }
      }),
      catchError((err, caught) => {
        return of(err);
      })
    );
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
    this.router.navigate(['/login']);
  }

  signup(data: Partial<User>): any {
    return this.baseApi.post(`/user/signup`, data).pipe(
      map((resp: any) => {
        if (resp.success) {
          return true;
        }
      }),
      catchError((err, caught) => {
        return of(err);
      })
    );
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

}
