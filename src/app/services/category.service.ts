import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Category } from '../models/category';
import { BaseHttpService } from './base-http.service';
@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  constructor(private baseApi: BaseHttpService) {}

  create(email: Partial<Category>): any {
    return this.baseApi.post(`/category`, email).pipe(
      map((resp: any) => {
        if (resp.success) {
          return resp.category;
        }
      }),
      catchError((err, caught) => {
        return of(err);
      })
    );
  }

  getById(id: string): any {
    return this.baseApi.get(`/category/${id}`).pipe(
      map((resp: any) => {
        if (resp.success) {
          return resp.category;
        }
      }),
      catchError((err, caught) => {
        return of(err);
      })
    );
  }

  getAllCategories(): any {
    return this.baseApi.get(`/category`).pipe(
      map((resp: any) => {
        if (resp.success) {
          return resp.categories;
        }
      }),
      catchError((err, caught) => {
        return of(err);
      })
    );
  }

  update(id: string, email: Partial<Category>): any {
    return this.baseApi.put(`/category/${id}`, email).pipe(
      map((resp: any) => {
        if (resp.success) {
          return true;
        }
      }),
      catchError((err, caught) => {
        return of(err);
      })
    );
  }

  delete(id: string): any {
    return this.baseApi.delete(`/category/${id}`).pipe(
      map((resp: any) => {
        if (resp.success) {
          return true;
        }
      }),
      catchError((err, caught) => {
        return of(err);
      })
    );
  }

}
