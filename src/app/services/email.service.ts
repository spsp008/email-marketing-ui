import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Email } from '../models/email';
import { BaseHttpService } from './base-http.service';
@Injectable({
  providedIn: 'root',
})
export class EmailService {
  constructor(private baseApi: BaseHttpService) {}

  create(email: Partial<Email>) {
    return this.baseApi.post(`/email`, email).pipe(
      map((resp: any) => {
        if (resp.success) {
          return resp.email;
        }
      }),
      catchError((err, caught) => {
        return of(err);
      })
    );
  }

  getById(id: string): any {
    return this.baseApi.get(`/email/${id}`).pipe(
      map((resp: any) => {
        if (resp.success) {
          return resp.email;
        }
      }),
      catchError((err, caught) => {
        return of(err);
      })
    );
  }

  getAll() {
    return this.baseApi.get(`/email`).pipe(
      map((resp: any) => {
        if (resp.success) {
          return resp.emails;
        }
      }),
      catchError((err, caught) => {
        return of(err);
      })
    );
  }

  update(id: string, email: Partial<Email>) {
    console.log(email);
    return this.baseApi.put(`/email/${id}`, email).pipe(
      map((resp: any) => {
        if (resp.success) {
          return true;
        }
      }),
      catchError((err, caught) => {
        return of(err);
      })
    );
  }

  delete(id: string) {
    return this.baseApi.delete(`/email/${id}`).pipe(
      map((resp: any) => {
        if (resp.success) {
          return true;
        }
      }),
      catchError((err, caught) => {
        return of(err);
      })
    );
  }

}
